#include "SqFunction.h"

#include <cstring>
#include <memory>
#include <mysql.h>

bool mysql_opt_reconnect = false;

int mysql_cached_errno = 0;
const SQChar* mysql_cached_error = "";

SQInteger sq_mysql_connect(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 7)
		return sq_throwerror(vm, "wrong number of parameters");

	const SQChar* sHost;
	sq_getstring(vm, 2, &sHost);

	const SQChar* sUser;
	sq_getstring(vm, 3, &sUser);

	const SQChar* sPass;
	sq_getstring(vm, 4, &sPass);

	const SQChar* sDatabase;
	sq_getstring(vm, 5, &sDatabase);

	SQInteger iPort = 3306;
	if (top == 6)
		sq_getinteger(vm, 6, &iPort);

	const SQChar *sSocket = nullptr;
	if (top == 7 && sq_gettype(vm, 7) == OT_STRING)
		sq_getstring(vm, 7, &sSocket);

	MYSQL* pHandler = mysql_init(NULL);

	if (!pHandler)
	{
		sq_pushnull(vm);
		return 1;
	}

	mysql_options(pHandler, MYSQL_OPT_RECONNECT, &mysql_opt_reconnect);

	if (!mysql_real_connect(pHandler, sHost, sUser, sPass, sDatabase, iPort, sSocket, 0))
	{
		mysql_cached_errno = mysql_errno(pHandler);
		mysql_cached_error = mysql_error(pHandler);

		mysql_close(pHandler);
		sq_pushnull(vm);

		return 1;
	}

	sq_pushuserpointer(vm, pHandler);
	return 1;
}

SQInteger sq_mysql_close(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (pPointer)
	{
		MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
		mysql_close(pHandler);
	}

	return 0;
}

SQInteger sq_mysql_select_db(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -2, &pPointer);

	const SQChar* sDb;
	sq_getstring(vm, -1, &sDb);

	bool result = false;
	if (pPointer)
	{
		MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
		result = mysql_select_db(pHandler, sDb) == 0;
	}

	sq_pushbool(vm, result);
	return 1;
}

SQInteger sq_mysql_query(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -2, &pPointer);

	const SQChar* sQuery;
	sq_getstring(vm, -1, &sQuery);
	
	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
	if (mysql_query(pHandler, sQuery))
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL_RES* pResult = mysql_store_result(pHandler);
	if (!pResult)
	{
		sq_pushnull(vm);
		return 1;
	}

	sq_pushuserpointer(vm, pResult);
	return 1;
}

SQInteger sq_mysql_insert_id(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
	sq_pushinteger(vm, mysql_insert_id(pHandler));

	return 1;
}

SQInteger sq_mysql_affected_rows(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
	sq_pushinteger(vm, mysql_affected_rows(pHandler));

	return 1;
}

SQInteger sq_mysql_num_rows(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL_RES* pResult = reinterpret_cast<MYSQL_RES*>(pPointer);
	sq_pushinteger(vm, mysql_num_rows(pResult));

	return 1;
}

SQInteger sq_mysql_num_fields(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL_RES* pResult = reinterpret_cast<MYSQL_RES*>(pPointer);
	sq_pushinteger(vm, mysql_num_fields(pResult));

	return 1;
}

SQInteger sq_mysql_fetch_row(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL_RES* pResult = reinterpret_cast<MYSQL_RES*>(pPointer);
	MYSQL_FIELD* pFields = mysql_fetch_fields(pResult);
	MYSQL_ROW pRow = mysql_fetch_row(pResult);

	if (!pFields || !pRow)
	{
		sq_pushnull(vm);
		return 1;
	}
	
	sq_newarray(vm, 0);

	for (unsigned int i = 0, numFields = mysql_num_fields(pResult); i < numFields; ++i)
	{
		enum_field_types fieldType = (pRow[i]) ? pFields[i].type : MYSQL_TYPE_NULL;

		switch (fieldType)
		{
			case MYSQL_TYPE_TINY:
			case MYSQL_TYPE_SHORT:
			case MYSQL_TYPE_LONG:
			case MYSQL_TYPE_LONGLONG:
			case MYSQL_TYPE_INT24:
			case MYSQL_TYPE_YEAR:
			case MYSQL_TYPE_BIT:
				sq_pushinteger(vm, atoi(pRow[i]));
				sq_arrayappend(vm, -2);
				break;

			case MYSQL_TYPE_NULL:
				sq_pushnull(vm);
				sq_arrayappend(vm, -2);
				break;

			case MYSQL_TYPE_DECIMAL:
			case MYSQL_TYPE_NEWDECIMAL:
			case MYSQL_TYPE_FLOAT:
			case MYSQL_TYPE_DOUBLE:
				sq_pushfloat(vm, atof(pRow[i]));
				sq_arrayappend(vm, -2);
				break;

			default:
				sq_pushstring(vm, pRow[i], -1);
				sq_arrayappend(vm, -2);
				break;
		}
	}

	return 1;
}

SQInteger sq_mysql_fetch_assoc(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL_RES* pResult = reinterpret_cast<MYSQL_RES*>(pPointer);
	MYSQL_FIELD* pFields = mysql_fetch_fields(pResult);
	MYSQL_ROW pRow = mysql_fetch_row(pResult);

	if (!pFields || !pRow)
	{
		sq_pushnull(vm);
		return 1;
	}
				
	sq_newtable(vm);

	for (unsigned int i = 0, numFields = mysql_num_fields(pResult); i < numFields; ++i)
	{
		enum_field_types fieldType = (pRow[i]) ? pFields[i].type : MYSQL_TYPE_NULL;

		switch (fieldType)
		{
			case MYSQL_TYPE_TINY:
			case MYSQL_TYPE_SHORT:
			case MYSQL_TYPE_LONG:
			case MYSQL_TYPE_LONGLONG:
			case MYSQL_TYPE_INT24:
			case MYSQL_TYPE_YEAR:
			case MYSQL_TYPE_BIT:
				sq_pushstring(vm, pFields[i].name, -1);
				sq_pushinteger(vm, atoi(pRow[i]));
				sq_newslot(vm, -3, SQFalse);
				break;

			case MYSQL_TYPE_NULL:
				sq_pushstring(vm, pFields[i].name, -1);
				sq_pushnull(vm);
				sq_newslot(vm, -3, SQFalse);
				break;

			case MYSQL_TYPE_DECIMAL:
			case MYSQL_TYPE_NEWDECIMAL:
			case MYSQL_TYPE_FLOAT:
			case MYSQL_TYPE_DOUBLE:
				sq_pushstring(vm, pFields[i].name, -1);
				sq_pushfloat(vm, atof(pRow[i]));
				sq_newslot(vm, -3, SQFalse);
				break;

			default:
				sq_pushstring(vm, pFields[i].name, -1);
				sq_pushstring(vm, pRow[i], -1);
				sq_newslot(vm, -3, SQFalse);
				break;
		}
	}

	return 1;
}

SQInteger sq_mysql_ping(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);
	
	bool result = false;
	if (pPointer)
	{
		MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
		result = !mysql_ping(pHandler);
	}

	sq_pushbool(vm, result);
	return 1;
}

SQInteger sq_mysql_free_result(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (pPointer)
	{
		MYSQL_RES* pResult = reinterpret_cast<MYSQL_RES*>(pPointer);
		mysql_free_result(pResult);
	}

	return 0;
}

SQInteger sq_mysql_error(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 2)
		return sq_throwerror(vm, "wrong number of parameters");

	if (top == 1)
	{
		sq_pushstring(vm, mysql_cached_error, -1);
		return 1;
	}
	
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
	sq_pushstring(vm, mysql_error(pHandler), -1);

	return 1;
}

SQInteger sq_mysql_errno(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 2)
		return sq_throwerror(vm, "wrong number of parameters");

	if (top == 1)
	{
		sq_pushinteger(vm, mysql_cached_errno);
		return 1;
	}

	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
	sq_pushinteger(vm, mysql_errno(pHandler));

	return 1;
}

SQInteger sq_mysql_info(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
	sq_pushstring(vm, mysql_info(pHandler), -1);

	return 1;
}

SQInteger sq_mysql_stat(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
	sq_pushstring(vm, mysql_stat(pHandler), -1);

	return 1;
}

SQInteger sq_mysql_sqlstate(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
	sq_pushstring(vm, mysql_sqlstate(pHandler), -1);

	return 1;
}

SQInteger sq_mysql_warning_count(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
	sq_pushinteger(vm, mysql_warning_count(pHandler));

	return 1;
}

SQInteger sq_mysql_get_character_set_info(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
	MY_CHARSET_INFO info;

	mysql_get_character_set_info(pHandler, &info);

	sq_newtable(vm);

	sq_pushstring(vm, "number", -1);
	sq_pushinteger(vm, info.number);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "state", -1);
	sq_pushinteger(vm, info.state);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "csname", -1);
	sq_pushstring(vm, info.csname, -1);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "name", -1);
	sq_pushstring(vm, info.name, -1);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "comment", -1);
	sq_pushstring(vm, info.comment, -1);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "dir", -1);
	sq_pushstring(vm, info.dir, -1);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "mbminlen", -1);
	sq_pushinteger(vm, info.mbminlen);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "mbmaxlen", -1);
	sq_pushinteger(vm, info.mbmaxlen);
	sq_newslot(vm, -3, SQFalse);
	
	return 1;
}

SQInteger sq_mysql_character_set_name(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
	sq_pushstring(vm, mysql_character_set_name(pHandler), -1);

	return 1;
}

SQInteger sq_mysql_set_character_set(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -2, &pPointer);

	const SQChar* sCharset;
	sq_getstring(vm, -1, &sCharset);

	bool result = false;
	if (pPointer)
	{
		MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
		result = (mysql_set_character_set(pHandler, sCharset) == 0);
	}

	sq_pushbool(vm, result);
	return 1;
}

SQInteger sq_mysql_escape_string(HSQUIRRELVM vm)
{
	const SQChar* sString;
	sq_getstring(vm, -1, &sString);

	size_t uStrLength = strlen(sString);
	if (uStrLength == 0)
	{
		sq_pushnull(vm);
		return 1;
	}

	std::unique_ptr<char[]> buffer(new char[uStrLength * 2 + 1]);
	unsigned long size = mysql_escape_string(buffer.get(), sString, uStrLength);

	if (size != -1)
		sq_pushstring(vm, buffer.get(), strlen(buffer.get()));
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_mysql_real_escape_string(HSQUIRRELVM vm)
{
	SQUserPointer pPointer;
	sq_getuserpointer(vm, -2, &pPointer);

	const SQChar* sString;
	sq_getstring(vm, -1, &sString);

	if (!pPointer)
	{
		sq_pushnull(vm);
		return 1;
	}

	size_t uStrLength = strlen(sString);
	if (uStrLength == 0)
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL* pHandler = reinterpret_cast<MYSQL*>(pPointer);
	std::unique_ptr<char[]> buffer(new char[uStrLength * 2 + 1]);
	unsigned long size = mysql_real_escape_string(pHandler, buffer.get(), sString, uStrLength);

	if (size != -1)
		sq_pushstring(vm, buffer.get(), strlen(buffer.get()));
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_mysql_option_reconnect(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 2)
		return sq_throwerror(vm, "wrong number of parameters");

	if (top == 1)
	{
		sq_pushbool(vm, mysql_opt_reconnect);
		return 1;
	}

	SQBool arg;
	sq_getbool(vm, -1, &arg);

	mysql_opt_reconnect = (arg == 1);
	return 0;
}
