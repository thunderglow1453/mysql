#include "SqFunction.h"
#include <sqrat.h>

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);
	Sqrat::DefaultVM::Set(vm);

	Sqrat::RootTable roottable(vm);

	/* squirreldoc (func)
	*
	* This function is used to open a connection to a MySQL server.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_connect
	* @param	(string) host the MySQL server host. It can also include a port number. e.g. "hostname:port" or a path to a local socket e.g. ":/path/to/socket" for the localhost.
	* @param	(string) username the username login.
	* @param	(string) password the password.
	* @param	(string) database the database name that will be used by the connection.
	* @param	(int) port=3306 the MySQL server port.
	* @param	(string) socket=null the MySQL socket file, e.g: `"/tmp/mysql.sock"`.
	* @return	(null|userpointer) returns a MySQL connection handler on success or null on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_connect", sq_mysql_connect, -5, ".ssssi");
	/* squirreldoc (func)
	*
	* This function is used to close MySQL connection.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_close
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	*
	*/
	roottable.SquirrelFunc("mysql_close", sq_mysql_close, 2, ".p");
	/* squirreldoc (func)
	*
	* This function is used to select a MySQL database for opened connection.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_select_db
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @param	(string) database the database name that will be used by the connection.
	*
	*/
	roottable.SquirrelFunc("mysql_select_db", sq_mysql_select_db, 3, ".ps");
	/* squirreldoc (func)
	*
	* This function is used to execute SQL query on MySQL server.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_query
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @param	(string) query the SQL query that will be executed on MySQL server.
	* @return	(null|userpointer) The result handler or `null` if query doesn't return any result.
	*
	*/
	roottable.SquirrelFunc("mysql_query", sq_mysql_query, 3, ".ps");
	/* squirreldoc (func)
	*
	* This function will get the ID generated in the last query.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_insert_id
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(null|int) The ID generated for an AUTO_INCREMENT column by the previous query on success, `0` if the previous query does not generate an AUTO_INCREMENT value, or `null` if no MySQL connection was established.
	*
	*/
	roottable.SquirrelFunc("mysql_insert_id", sq_mysql_insert_id, 2, ".p");
	/* squirreldoc (func)
	*
	* This function retrieves the number of affected rows from a result handler.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_affected_rows
	* @param	(userpointer) result the query result handler which comes from a call to [mysql_query](../mysql_query).
	* @return	(int) Returns the number of affected rows on success, and -1 if the last query failed.
	*
	*/
	roottable.SquirrelFunc("mysql_affected_rows", sq_mysql_affected_rows, 2, ".p");
	/* squirreldoc (func)
	*
	* This function retrieves the number of rows from a result handler.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_num_rows
	* @param	(userpointer) result the query result handler which comes from a call to [mysql_query](../mysql_query).
	* @return	(null|int) The ID generated for an AUTO_INCREMENT column by the previous query on success, `0` if the previous query does not generate an AUTO_INCREMENT value, or `null` if no MySQL connection was established.
	*
	*/
	roottable.SquirrelFunc("mysql_num_rows", sq_mysql_num_rows, 2, ".p");
	/* squirreldoc (func)
	*
	* This function retrieves the number of fields from a result handler.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_num_fields
	* @param	(userpointer) result the query result handler which comes from a call to [mysql_query](../mysql_query).
	* @return	(null|int) number of fields in the result handler, or `null` if no MySQL connection was established.
	*
	*/
	roottable.SquirrelFunc("mysql_num_fields", sq_mysql_num_fields, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets a result row as an array.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_fetch_row
	* @param	(userpointer) result the query result handler which comes from a call to [mysql_query](../mysql_query).
	* @return	(null|array) array of values that corresponds to the fetched row, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_fetch_row", sq_mysql_fetch_row, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets a result row as a table.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_fetch_assoc
	* @param	(userpointer) result the query result handler which comes from a call to [mysql_query](../mysql_query).
	* @return	(null|table) table of values with string indicies as row names that corresponds to the fetched row, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_fetch_assoc", sq_mysql_fetch_assoc, 2, ".p");
	/* squirreldoc (func)
	*
	* This function pings a server connection or reconnect if there is no connection.
	*
	* @version	0.2
	* @side		shared
	* @note		The reconnect feature is disabled by default, and can be enabled via [mysql_option_reconnect](../mysql_option_reconnect).
	* @name		mysql_ping
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(bool) `true` if the connection to the server MySQL server is working, otherwise `false`.
	*
	*/
	roottable.SquirrelFunc("mysql_ping", sq_mysql_ping, 2, ".p");
	/* squirreldoc (func)
	*
	* This function will free result memory.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_free_result
	* @param	(userpointer) result the query result handler which comes from a call to [mysql_query](../mysql_query).
	*
	*/
	roottable.SquirrelFunc("mysql_free_result", sq_mysql_free_result, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets the text of the error message from previous MySQL operation.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_error
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(null|string) the error text from the last MySQL function, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_error", sq_mysql_error, -1, ".p");
	/* squirreldoc (func)
	*
	* This function gets the error code from previous MySQL operation.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_errno
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(null|int) the error code from the last MySQL function, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_errno", sq_mysql_errno, -1, ".p");
	/* squirreldoc (func)
	*
	* This function gets the information about the most recent query.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_info
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(null|string) information about the statement on success, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_info", sq_mysql_info, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets the current system status.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_stat
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(null|string) a string with the status for uptime, threads, queries, open tables, flush tables and queries per second, or `null` on failure. For a complete list of other status variables, you have to use the `SHOW STATUS` SQL command.
	*
	*/
	roottable.SquirrelFunc("mysql_stat", sq_mysql_stat, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets the text of the error message for the most recently executed SQL statement.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_sqlstate
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(null|string) the error text from recently executed SQL statement, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_sqlstate", sq_mysql_sqlstate, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets the number of errors, warnings, and notes generated during execution of the previous SQL statement.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_warning_count
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(null|int) the warning count, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_warning_count", sq_mysql_warning_count, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets the client character set info.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_get_character_set_info
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(null|{number, state, csname, name, comment, dir, mbminlen, mbmaxlen}) the info about currently used character set by the connection, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_get_character_set_info", sq_mysql_get_character_set_info, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets the client character set.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_character_set_name
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(null|string) the current used character set by the connection, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_character_set_name", sq_mysql_character_set_name, 2, ".p");
	/* squirreldoc (func)
	*
	* This function sets the client character set.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_set_character_set
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @param	(string) charset a valid character set name.
	* @return	(bool) `true` if the charset was successfully set, otherwise `false`.
	*
	*/
	roottable.SquirrelFunc("mysql_set_character_set", sq_mysql_set_character_set, 3, ".ps");
	/* squirreldoc (func)
	*
	* This function escapes special characters in a string for use in [mysql_query](../mysql_query).
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_escape_string
	* @param	(string) unescaped_string the string that is to be escaped.
	* @return	(null|string) the escaped string, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_escape_string", sq_mysql_escape_string, 2, ".s");
	/* squirreldoc (func)
	*
	* This function escapes special characters in a string for use in [mysql_query](../mysql_query).  
	* It is identical to [mysql_escape_string](../mysql_escape_string) except that it takes a connection handler and escapes the string according to the current character set.
	*
	* @version	0.3
	* @side		shared
	* @name		mysql_real_escape_string
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @param	(string) unescaped_string the string that is to be escaped.
	* @return	(null|string) the escaped string, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_real_escape_string", sq_mysql_real_escape_string, 3, ".ps");
	/* squirreldoc (func)
	*
	* This function gets or sets the mysql reconnect option.
	*
	* @version	0.4.2
	* @side		shared
	* @note		You should call a setter before creating new connections via [mysql_connect](../mysql_connect).
	* @name		mysql_option_reconnect
	* @param	(bool) reconnect=optional This parameter is optional, you have to specify it to call a setter. Pass `true` if you want to enable built in mysql reconnect, otherwise `false`.
	* @return	(void|bool) nothing or `bool` if you've called getter.
	*
	*/
	roottable.SquirrelFunc("mysql_option_reconnect", sq_mysql_option_reconnect, -1, ".b");

	return SQ_OK;
}
