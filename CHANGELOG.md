## Changelog

- Downgraded build image from `ubuntu 22.04` to `ubuntu 20.04`, which means that the version of glibc required to run the module should also be lower.